# tranSMART-dataquality

This repository is the source code for Khalid Yusuf's master thesis. 

The project creates different workflows required to assess the quality of categorical data including: 
 * `Barchart` workflow to render missing values per element.
 * `TwoWayPlausibilitiy` workflow to check for implausible values between two interdependent biomaterials e.g. "numfullserum" and "numfullserumaliquot", "numfullserumaliquot" and "numaliquotserumcount"
 * `ThreeWayPlausibilitiy` workflow to check for implausible values within three absolutely interdependent states of urine sample - "stateurinordinary", "stateurinopaque" and "stateurinbloody"
 * `Contingency` workflow to check for implausible values within four absolutely interdependent states of blood sample - "stateordinary", "stateikteric", "statehaemolytic" and "statelipaemic" 
 * `Dataquality` workflow to render tabular assessment report which captures overall plausibility ratings. All biomaterials including serum, citrat, edta and urine should be selected. All urine and blood states should be selected.

These plugins have been incorporated in the `release 16.2` of the SmartR sourcecode of the tranSMART analysis platform. 

The details of how to build the `.war` is provided below:

## Building the `.war`
### Approach 1: build `.war` on PC or Docker using Grails
The `SmartR` repository here has to be cloned together with the following repositories into a local directory. All 4 repositories below can be sourced from `release 16.2` branch of tranSMART-Foundation repositories [4]. 
 * `transmart-data`
 * `Rmodules`
 * `transmart-core-db`
 * `transmartApp` 

Thereafter, change to the `transmartApp` directory on your local setup and run grails war.

The new tranSMART war can then be deployed in a tranSMART setup. 

### Approach 2: use UMG-internal GitLab-CI/CD Pipeline
_THIS APPROACH DOES ONLY WORK WITH VALID CREDENTIALS FOR GWDG-GITLAB!_ 
  
* Step 1: create a personal GitLab branch in each of the following Repositories [1]
  * `transmart-data`
  * `Rmodules`
  * `transmart-core-db`
  * `transmartApp`
  * `SmartR`
* Step 2: copy/move the plugin files into the `SmartR`-Repository [2]
* Step 3: modify `.gitlab-ci.yml` File
  * replace branch "master" by your personal branch name
* Step 4: start the CI/CD Pipeline (if not triggered automatically)
* Step 5: Start the tranSMART-server by using the `tm_umg`-Repository from GWDG-GitLab [3]
  * clone `tm_umg`-Repository
  * switch to your (local) personal branch
  * modify `.env` according to `tm_umg`-README --> replace WAR_BRANCH by personal branch name
  * start the server via ```docker-compose up -d```


______
[1] [https://gitlab.gwdg.de/medinf/AGITF/transmart](https://gitlab.gwdg.de/medinf/AGITF/transmart)  
[2] [https://gitlab.gwdg.de/medinf/AGITF/transmart/SmartR](https://gitlab.gwdg.de/medinf/AGITF/transmart/SmartR)  
[3] [https://gitlab.gwdg.de/medinf/AGITF/tm_umg](https://gitlab.gwdg.de/medinf/AGITF/tm_umg)

[4] (https://github.com/tranSMART-Foundation)
