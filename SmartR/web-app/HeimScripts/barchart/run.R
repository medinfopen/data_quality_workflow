library("jsonlite")
##Input-Daten = variable 'loaded_variables'
main <- function() {
          #uebergebene Parameter zwischenspeichern
          tmp.params <- fetch_params

  #uebergebene Variablen zwischenspeichern und zu DataFrame konvertieren
  tmp.var1 <- unlist(loaded_variables, recursive = FALSE)
    tmp.var1 <- as.data.frame(tmp.var1)

    #Duplikat-Spalten entfernen
    tmp.delColumns <- grep("Row.Label", colnames(tmp.var1))
      tmp.delColumns <- tmp.delColumns[2:length(tmp.delColumns)]
      tmp.var2 <- tmp.var1[,-tmp.delColumns]

        #temporaere Dateien freigeben
        remove(tmp.var1, tmp.delColumns)

        number <- length(tmp.params$ontologyTerms)

          tmp.nameList = character(number)
          tmp.keyList = character(number)
            for (i in 1:number) {
                        tmp.subIndex <- gregexpr(pattern = "\\\\", tmp.params$ontologyTerms[[i]]$key)[[1]]
              tmp.subLength <- length(tmp.subIndex)

                  tmp.malus <- 0;
                  if (is.null(tmp.params$ontologyTerms[[i]]$metadata$unitValues$normalUnits)) {tmp.malus <- 1}

                  tmp.nameList[i] <- substr(tmp.params$ontologyTerms[[i]]$key, tmp.subIndex[tmp.subLength-1-tmp.malus]+1, tmp.subIndex[tmp.subLength-tmp.malus]-1)
                     tmp.keyList[i] <- substr(tmp.params$ontologyTerms[[i]]$key, tmp.subIndex[3], tmp.subIndex[tmp.subLength-1])
                       }
           tmp.nameList <- unique(tmp.nameList);
             tmp.keyList <- unique(tmp.keyList)

             ## Format to tmp.var2 design
             tmp.keyList <- gsub(" ", ".", tmp.keyList)
               tmp.keyList <- gsub("\\(", ".", tmp.keyList)
               tmp.keyList <- gsub("\\)", ".", tmp.keyList)
                 tmp.keyList <- gsub("-", ".", tmp.keyList)
                 tmp.keyList <- gsub("\\/", ".", tmp.keyList)
                   tmp.keyList <- gsub("\\\\", ".", tmp.keyList)

                   colnames(tmp.var2)[1] <- "internalId"


                       for (i in 1:length(tmp.keyList)) {
                                   tmp.currentCols <- grep(tmp.keyList[i], colnames(tmp.var2))
                       tmp.var2[,tmp.currentCols[1]] <- as.character(tmp.var2[,tmp.currentCols[1]])

                           if (length(tmp.currentCols) > 1) {
                                         for (idx in tmp.currentCols[2:length(tmp.currentCols)]) {
                                                         tmp.var2[,tmp.currentCols[1]] <- ifelse(is.na(tmp.var2[,tmp.currentCols[1]]) | tmp.var2[,tmp.currentCols[1]] == "", as.character(tmp.var2[,idx]), tmp.var2[,tmp.currentCols[1]])
                                                         }

                        tmp.var2 <- tmp.var2[,-tmp.currentCols[2:length(tmp.currentCols)]]
                                 }



                        colnames(tmp.var2)[tmp.currentCols[1]] <- gsub(" ", "", tmp.nameList[i])
                               }
                     output <- calculate_dataquality(tmp.var2)
                     output_json <- toJSON(output, na = "string")
                    # output_json
                     write(output_json, "/tmp/exportpie.json")
                     output_json
}
calculate_dataquality <- function(input_data){
# subset
      library(dplyr)
	    input_data[input_data==""]<-NA
      splitted_input <- split_dataset(input_data = input_data)
      input_data_numeric <- splitted_input$input_data_numeric
      input_data_categorical <- splitted_input$input_data_categorical

      catNAs <- missing_record(input_data_categorical)
      unknown <- unknown_record(input_data_categorical)

         
      return_value <- list("catNAs"=catNAs, "unknown"=unknown)
      
      return(return_value)
}

#eigene Funktionen, tmp.var2 ist Haupt-Variable (input_data)
split_dataset <- function(input_data){
    ## Schritt 0.2: Aufteilen in numerische und kategorielle Daten
      isnumeric <- sapply(input_data, is.numeric)
      input_data_numeric <- input_data[,isnumeric]
      input_data_categorical <- input_data[,!isnumeric]
      rm(isnumeric) #entfernen d. temporaeren Variable
      
      return_value <- list("input_data_numeric"=input_data_numeric, "input_data_categorical"=input_data_categorical)
     
      return(return_value)
}


# get count of missing values per element
missing_record <- function(df) {
  missingrecord <- sapply(df, function(df) sum(length(which(is.na(df)))))
  record_missing <- as.data.frame(missingrecord)
  rm(missingrecord)
  missing_field <- cbind(row.names(record_missing), record_missing)
  colnames(missing_field) <- c("variables", "missing_count")
  
  return(missing_field)
  
}

# get unknown values count per element
unknown_record <- function(df) {
  unknownrecord <- sapply(df, function(df) sum(length(which(df=="nicht erhoben" | df=="unbekannt"))))
  record_df <- as.data.frame(unknownrecord)
  rm(unknownrecord)
  record_unbekannt <- cbind(row.names(record_df), record_df)
  colnames(record_unbekannt) <- c("variables", "unknown_count")
  
  return(record_unbekannt)
  
}
