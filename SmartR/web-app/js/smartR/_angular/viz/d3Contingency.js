//# sourceURL=d3Contingency.js

'use strict';

window.smartRApp.directive('contingency', [
    'smartRUtils',
    'rServeService',
    function(smartRUtils, rServeService) {

        return {
            restrict: 'E',
            scope: {
                data: '=',
                width: '@',
                height: '@'
            },
            link: function (scope, element) {

                /**
                 * Watch data model (which is only changed by ajax calls when we want to (re)draw everything)
                 */
                scope.$watch('data', function() {
                    $(element[0]).empty();
                    if (! $.isEmptyObject(scope.data)) {
                        smartRUtils.prepareWindowSize(scope.width, scope.height);
                        createContingencyViz(scope, element[0]);
                    }
                });
            }
            };

       function createContingencyViz(scope, root) {
           var df = scope.data.input_data;
           //var df_column = d3.keys(df[0]);
	         var df2 = scope.data.second_data;
	       //var df_column = d3.keys(df[0]);
           var ont = scope.data.ontologies;
           var xArrLabel = scope.data.ont1;
           var yArrLabel = scope.data.ont2;
           var zArrLabel = scope.data.ont3;
           var wLabel = scope.data.ont4;


           var margin = {top: 30, right: 130, bottom: 30, left: 120},
          // var margin = {top: 20, right: 250, bottom: h / 4 + 230, left: w / 4},
               width = 960 - margin.left - margin.right,
               height = 500 - margin.top - margin.bottom;

               var x = d3.scale.ordinal().rangeBands([0, width]),
                   y = d3.scale.ordinal().rangeBands([height, 0]),
                   z = d3.scale.linear().range(["lightblue", "brown"]).interpolate(d3.interpolateHcl);
               var w = d3.scale.category10();

               // plot one
               var svg = d3.select(root).append("svg")
                                       .attr("width", width + margin.left + margin.right)
                                       .attr("height", height + margin.top + margin.bottom)
                                       .append("g")
                                       .attr("transform", "translate(" + margin.left + "," + margin.top
                                                                        + ")");
                                x.domain(df.map(function(d) { return d.var1; }));
                                y.domain(df.map(function(d) { return d.var2; }));

                                svg.selectAll(".tile")
                                           .data(df)
                                           .enter().append("rect")
                                           .attr("class", "tile")
                                           .attr("x", function(d) {
                                                console.log(d.var1+"haha"+x(d.var1));
                                                console.log(x.rangeBand(x.var1));
                                                   return x(d.var1); })
                                            .attr("y", function(d) {
                                                                            return y(d.var2); })
                                                //.attr("width", width + margin.left + margin.right)
                                                //.attr("height", height + margin.top + margin.bottom)
                                            .attr("width", x.rangeBand(x.var1))
                                            .attr("height",  y.rangeBand(y.var2))
                                            //.attr("height",  z.rangeBand(z.var4))
                                            .style("fill", function(d) { return w(d.plausibility_code); })
                                            .append("title")
                                            .text(function(d){return d.plausibility_code});
                var legend = svg.selectAll(".legend")
                                                 .data(w.domain())
                                                 .enter().append("g")
                                                 .attr("class", "legend")
                                                 .attr("transform", function(d, i) { return "translate(" + (width +                                                                               20) + "," + (20 + i * 20) + ")"; });
                              legend.append("rect")
                                                 .attr("width", 20)
                                                 .attr("height", 20)
                                                 .style("fill", function (d) {return w(d);});
                              legend.append("text")
                                                 .attr("x", 26)
                                                 .attr("y", 10)
                                                 .attr("dy", ".35em")
                                                 .text(function(d) {return d;});
                              svg.append("text")
                                             .attr("class", "label")
                                             .attr("x", width + 20)
                                             .attr("y", 10)
                                             .attr("dy", ".35em")
                                             .style("font-size", "14px")
                                             .style("text-decoration", "bold")
                                             .text("Plausibility_code");
                             svg.append("g")
                                           .attr("class", "x axis")
                                           .attr("transform", "translate(0," + height + ")")
                                           .call(d3.svg.axis().scale(x).orient("bottom"))
                                           .append("text")
                                           .attr("class", "label")
                                           .attr("x", width)
                                           .attr("y", -6)
                                           .attr("text-anchor", "end")
                                           .style("font-size", "14px")
                                           .style("text-decoration", "bold")
                                           .text(ont[0]);
                              svg.append("g")
                                               .attr("class", "y axis")
                                               .call(d3.svg.axis().scale(y).orient("left"))
                                               .append("text")
                                               .attr("class", "label")
                                               .attr("y", 6)
                                               .attr("dy", ".71em")
                                               .attr("text-anchor", "end")
                                               .attr("transform", "rotate(-90)")
                                               .style("font-size", "14px")
                                               .style("text-decoration", "bold")
                                               .text(ont[1]);
                              svg.append("text")
                                        .attr("x", (width / 2))
                                        .attr("y", 0 -(margin.top / 2))
                                        .style("font-size", "18px")
                                        .style("text-decoration", "bold")
                                        .attr("text-anchor", "middle")
                                        .text("Plausibility Check between Four Variables");
                // second chart
                var svg2 = d3.select(root).append("svg")
                                        .attr("width", width + margin.left + margin.right)
                                        .attr("height", height + margin.top + margin.bottom)
                                        .append("g")
                                        .attr("transform", "translate(" + margin.left + "," + margin.top
                                                                         + ")");
                                 x.domain(df.map(function(d) { return d.var1; }));
                                 y.domain(df.map(function(d) { return d.var3; }));

                                 svg2.selectAll(".tile")
                                            .data(df)
                                            .enter().append("rect")
                                            .attr("class", "tile")
                                            .attr("x", function(d) {
                                                 console.log(d.var1+"haha"+x(d.var1));
                                                 console.log(x.rangeBand(x.var1));
                                                    return x(d.var1); })
                                             .attr("y", function(d) {
                                                     return y(d.var3); })
                                             .attr("width", x.rangeBand(x.var1))
                                             .attr("height",  y.rangeBand(y.var3))
                                             //.attr("height",  z.rangeBand(z.var4))
                                             .style("fill", function(d) { return w(d.plausibility_code); })
                                             .append("title")
                                             .text(function(d){return d.plausibility_code});
                 var legend2 = svg2.selectAll(".legend")
                                                  .data(w.domain())
                                                  .enter().append("g")
                                                  .attr("class", "legend")
                                                  .attr("transform", function(d, i) { return "translate(" + (width +                                                                               20) + "," + (20 + i * 20) + ")"; });
                               legend2.append("rect")
                                                  .attr("width", 20)
                                                  .attr("height", 20)
                                                  .style("fill", function (d) {return w(d);});
                               legend2.append("text")
                                                  .attr("x", 26)
                                                  .attr("y", 10)
                                                  .attr("dy", ".35em")
                                                  .text(function(d) {return d;});
                               svg2.append("text")
                                              .attr("class", "label")
                                              .attr("x", width + 20)
                                              .attr("y", 10)
                                              .attr("dy", ".35em")
                                              .style("font-size", "14px")
                                              .style("text-decoration", "bold")
                                              .text("Plausibility_code");
                              svg2.append("g")
                                            .attr("class", "x axis")
                                            .attr("transform", "translate(0," + height + ")")
                                            .call(d3.svg.axis().scale(x).orient("bottom"))
                                            .append("text")
                                            .attr("class", "label")
                                            .attr("x", width)
                                            .attr("y", -6)
                                            .attr("text-anchor", "end")
                                            .style("font-size", "14px")
                                            .style("text-decoration", "bold")
                                            .text(ont[0]);
                               svg2.append("g")
                                                .attr("class", "y axis")
                                                .call(d3.svg.axis().scale(y).orient("left"))
                                                .append("text")
                                                .attr("class", "label")
                                                .attr("y", 6)
                                                .attr("dy", ".71em")
                                                .attr("text-anchor", "end")
                                                .attr("transform", "rotate(-90)")
                                                .style("font-size", "14px")
                                                .style("text-decoration", "bold")
                                                .text(ont[2]);

                  // 3rd chart
                  var svg3 = d3.select(root).append("svg")
                                          .attr("width", width + margin.left + margin.right)
                                          .attr("height", height + margin.top + margin.bottom)
                                          .append("g")
                                          .attr("transform", "translate(" + margin.left + "," + margin.top
                                                                           + ")");
                                   x.domain(df.map(function(d) { return d.var1; }));
                                   y.domain(df.map(function(d) { return d.var4; }));

                                   svg3.selectAll(".tile")
                                              .data(df)
                                              .enter().append("rect")
                                              .attr("class", "tile")
                                              .attr("x", function(d) {
                                                   console.log(d.var1+"haha"+x(d.var1));
                                                   console.log(x.rangeBand(x.var1));
                                                      return x(d.var1); })
                                               .attr("y", function(d) {
                                                            return y(d.var4); })
                                               .attr("width", x.rangeBand(x.var1))
                                               .attr("height",  y.rangeBand(y.var4))
                                               //.attr("height",  z.rangeBand(z.var4))
                                               .style("fill", function(d) { return w(d.plausibility_code); })
                                               .append("title")
                                               .text(function(d){return d.plausibility_code});
                   var legend3 = svg3.selectAll(".legend")
                                                    .data(w.domain())
                                                    .enter().append("g")
                                                    .attr("class", "legend")
                                                    .attr("transform", function(d, i) { return "translate(" + (width +                                                                               20) + "," + (20 + i * 20) + ")"; });
                                 legend3.append("rect")
                                                    .attr("width", 20)
                                                    .attr("height", 20)
                                                    .style("fill", function (d) {return w(d);});
                                 legend3.append("text")
                                                    .attr("x", 26)
                                                    .attr("y", 10)
                                                    .attr("dy", ".35em")
                                                    .text(function(d) {return d;});
                                 svg3.append("text")
                                                .attr("class", "label")
                                                .attr("x", width + 20)
                                                .attr("y", 10)
                                                .attr("dy", ".35em")
                                                .style("font-size", "14px")
                                                .style("text-decoration", "bold")
                                                .text("Plausibility_code");
                                svg3.append("g")
                                              .attr("class", "x axis")
                                              .attr("transform", "translate(0," + height + ")")
                                              .call(d3.svg.axis().scale(x).orient("bottom"))
                                              .append("text")
                                              .attr("class", "label")
                                              .attr("x", width)
                                              .attr("y", -6)
                                              .attr("text-anchor", "end")
                                              .style("font-size", "14px")
                                              .style("text-decoration", "bold")
                                              .text(ont[0]);
                                 svg3.append("g")
                                                  .attr("class", "y axis")
                                                  .call(d3.svg.axis().scale(y).orient("left"))
                                                  .append("text")
                                                  .attr("class", "label")
                                                  .attr("y", 6)
                                                  .attr("dy", ".71em")
                                                  .attr("text-anchor", "end")
                                                  .attr("transform", "rotate(-90)")
                                                  .style("font-size", "14px")
                                                  .style("text-decoration", "bold")
                                                  .text(ont[3]);

               // 4th chart
               var svg4 = d3.select(root).append("svg")
                                       .attr("width", width + margin.left + margin.right)
                                       .attr("height", height + margin.top + margin.bottom)
                                       .append("g")
                                       .attr("transform", "translate(" + margin.left + "," + margin.top
                                                                        + ")");
                                x.domain(df.map(function(d) { return d.var2; }));
                                y.domain(df.map(function(d) { return d.var3; }));

                                svg4.selectAll(".tile")
                                           .data(df)
                                           .enter().append("rect")
                                           .attr("class", "tile")
                                           .attr("x", function(d) {
                                                console.log(d.var1+"haha"+x(d.var2));
                                                console.log(x.rangeBand(x.var2));
                                                   return x(d.var2); })
                                            .attr("y", function(d) {
                                                                            return y(d.var3); })
                                                //.attr("width", width + margin.left + margin.right)
                                                //.attr("height", height + margin.top + margin.bottom)
                                            .attr("width", x.rangeBand(x.var2))
                                            .attr("height",  y.rangeBand(y.var3))
                                            //.attr("height",  z.rangeBand(z.var4))
                                            .style("fill", function(d) { return w(d.plausibility_code); })
                                            .append("title")
                                            .text(function(d){return d.plausibility_code});
                var legend4 = svg4.selectAll(".legend")
                                                 .data(w.domain())
                                                 .enter().append("g")
                                                 .attr("class", "legend")
                                                 .attr("transform", function(d, i) { return "translate(" + (width +                                                                               20) + "," + (20 + i * 20) + ")"; });
                              legend4.append("rect")
                                                 .attr("width", 20)
                                                 .attr("height", 20)
                                                 .style("fill", function (d) {return w(d);});
                              legend4.append("text")
                                                 .attr("x", 26)
                                                 .attr("y", 10)
                                                 .attr("dy", ".35em")
                                                 .text(function(d) {return d;});
                              svg4.append("text")
                                             .attr("class", "label")
                                             .attr("x", width + 20)
                                             .attr("y", 10)
                                             .attr("dy", ".35em")
                                             .style("font-size", "14px")
                                             .style("text-decoration", "bold")
                                             .text("Plausibility_code");
                             svg4.append("g")
                                           .attr("class", "x axis")
                                           .attr("transform", "translate(0," + height + ")")
                                           .call(d3.svg.axis().scale(x).orient("bottom"))
                                           .append("text")
                                           .attr("class", "label")
                                           .attr("x", width)
                                           .attr("y", -6)
                                           .attr("text-anchor", "end")
                                           .style("font-size", "14px")
                                           .style("text-decoration", "bold")
                                           .text(ont[1]);
                              svg4.append("g")
                                               .attr("class", "y axis")
                                               .call(d3.svg.axis().scale(y).orient("left"))
                                               .append("text")
                                               .attr("class", "label")
                                               .attr("y", 6)
                                               .attr("dy", ".71em")
                                               .attr("text-anchor", "end")
                                               .attr("transform", "rotate(-90)")
                                               .style("font-size", "14px")
                                               .style("text-decoration", "bold")
                                               .text(ont[2]);

               // 5th chart
               var svg5 = d3.select(root).append("svg")
                                       .attr("width", width + margin.left + margin.right)
                                       .attr("height", height + margin.top + margin.bottom)
                                       .append("g")
                                       .attr("transform", "translate(" + margin.left + "," + margin.top
                                                                        + ")");
                                x.domain(df.map(function(d) { return d.var2; }));
                                y.domain(df.map(function(d) { return d.var4; }));

                                svg5.selectAll(".tile")
                                           .data(df)
                                           .enter().append("rect")
                                           .attr("class", "tile")
                                           .attr("x", function(d) {
                                                console.log(d.var1+"haha"+x(d.var2));
                                                console.log(x.rangeBand(x.var2));
                                                   return x(d.var2); })
                                            .attr("y", function(d) {
                                                                            return y(d.var4); })
                                                //.attr("width", width + margin.left + margin.right)
                                                //.attr("height", height + margin.top + margin.bottom)
                                            .attr("width", x.rangeBand(x.var2))
                                            .attr("height",  y.rangeBand(y.var4))
                                            //.attr("height",  z.rangeBand(z.var4))
                                            .style("fill", function(d) { return w(d.plausibility_code); })
                                            .append("title")
                                            .text(function(d){return d.plausibility_code});
                var legend5 = svg5.selectAll(".legend")
                                                 .data(w.domain())
                                                 .enter().append("g")
                                                 .attr("class", "legend")
                                                 .attr("transform", function(d, i) { return "translate(" + (width +                                                                               20) + "," + (20 + i * 20) + ")"; });
                              legend5.append("rect")
                                                 .attr("width", 20)
                                                 .attr("height", 20)
                                                 .style("fill", function (d) {return w(d);});
                              legend5.append("text")
                                                 .attr("x", 26)
                                                 .attr("y", 10)
                                                 .attr("dy", ".35em")
                                                 .text(function(d) {return d;});
                              svg5.append("text")
                                             .attr("class", "label")
                                             .attr("x", width + 20)
                                             .attr("y", 10)
                                             .attr("dy", ".35em")
                                             .style("font-size", "14px")
                                             .style("text-decoration", "bold")
                                             .text("Plausibility_code");
                             svg5.append("g")
                                           .attr("class", "x axis")
                                           .attr("transform", "translate(0," + height + ")")
                                           .call(d3.svg.axis().scale(x).orient("bottom"))
                                           .append("text")
                                           .attr("class", "label")
                                           .attr("x", width)
                                           .attr("y", -6)
                                           .attr("text-anchor", "end")
                                           .style("font-size", "14px")
                                           .style("text-decoration", "bold")
                                           .text(ont[1]);
                              svg5.append("g")
                                               .attr("class", "y axis")
                                               .call(d3.svg.axis().scale(y).orient("left"))
                                               .append("text")
                                               .attr("class", "label")
                                               .attr("y", 6)
                                               .attr("dy", ".71em")
                                               .attr("text-anchor", "end")
                                               .attr("transform", "rotate(-90)")
                                               .style("font-size", "14px")
                                               .style("text-decoration", "bold")
                                               .text(ont[3]);

                 // 6th chart
                 var svg6 = d3.select(root).append("svg")
                                         .attr("width", width + margin.left + margin.right)
                                         .attr("height", height + margin.top + margin.bottom)
                                         .append("g")
                                         .attr("transform", "translate(" + margin.left + "," + margin.top
                                                                          + ")");
                                  x.domain(df.map(function(d) { return d.var3; }));
                                  y.domain(df.map(function(d) { return d.var4; }));

                                  svg6.selectAll(".tile")
                                             .data(df)
                                             .enter().append("rect")
                                             .attr("class", "tile")
                                             .attr("x", function(d) {
                                                  console.log(d.var1+"haha"+x(d.var3));
                                                  console.log(x.rangeBand(x.var3));
                                                     return x(d.var3); })
                                              .attr("y", function(d) {
                                                                              return y(d.var4); })
                                                  //.attr("width", width + margin.left + margin.right)
                                                  //.attr("height", height + margin.top + margin.bottom)
                                              .attr("width", x.rangeBand(x.var3))
                                              .attr("height",  y.rangeBand(y.var4))
                                              //.attr("height",  z.rangeBand(z.var4))
                                              .style("fill", function(d) { return w(d.plausibility_code); })
                                              .append("title")
                                              .text(function(d){return d.plausibility_code});
                  var legend6 = svg6.selectAll(".legend")
                                                   .data(w.domain())
                                                   .enter().append("g")
                                                   .attr("class", "legend")
                                                   .attr("transform", function(d, i) { return "translate(" + (width +                                                                               20) + "," + (20 + i * 20) + ")"; });
                                legend6.append("rect")
                                                   .attr("width", 20)
                                                   .attr("height", 20)
                                                   .style("fill", function (d) {return w(d);});
                                legend6.append("text")
                                                   .attr("x", 26)
                                                   .attr("y", 10)
                                                   .attr("dy", ".35em")
                                                   .text(function(d) {return d;});
                                svg6.append("text")
                                               .attr("class", "label")
                                               .attr("x", width + 20)
                                               .attr("y", 10)
                                               .attr("dy", ".35em")
                                               .style("font-size", "14px")
                                               .style("text-decoration", "bold")
                                               .text("Plausibility_code");
                               svg6.append("g")
                                             .attr("class", "x axis")
                                             .attr("transform", "translate(0," + height + ")")
                                             .call(d3.svg.axis().scale(x).orient("bottom"))
                                             .append("text")
                                             .attr("class", "label")
                                             .attr("x", width)
                                             .attr("y", -6)
                                             .attr("text-anchor", "end")
                                             .style("font-size", "14px")
                                             .style("text-decoration", "bold")
                                             .text(ont[2]);
                                svg6.append("g")
                                                 .attr("class", "y axis")
                                                 .call(d3.svg.axis().scale(y).orient("left"))
                                                 .append("text")
                                                 .attr("class", "label")
                                                 .attr("y", 6)
                                                 .attr("dy", ".71em")
                                                 .attr("text-anchor", "end")
                                                 .attr("transform", "rotate(-90)")
                                                 .style("font-size", "14px")
                                                 .style("text-decoration", "bold")
                                                 .text(ont[3]);


}
}
]);
