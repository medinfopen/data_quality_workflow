//# sourceURL=d3Dataquality.js
'use strict';

window.smartRApp.directive('dataquality', [
    'smartRUtils',
    'rServeService',
    '$rootScope',
    function(smartRUtils, rServeService, $rootScope) {
        return {
            restrict: 'E',
            scope: {
                data: '='
            },
            templateUrl: $rootScope.smartRPath +  '/js/smartR/_angular/templates/dataquality.html',
            link: function (scope, element) {
                var vizDiv = element[0];
                /**
                 * Watch data model (which is only changed by ajax calls when we want to (re)draw everything)
                 */
                scope.$watch('data', function () {
                    $(vizDiv).empty();
                    if (! $.isEmptyObject(scope.data)) {
                        smartRUtils.prepareWindowSize(scope.width, scope.height);
                        reportDataquality(scope, vizDiv);
                    }
                });
            }
        };
                function reportDataquality(scope, root) {
                  var blood_anomaly = scope.data.blood_anomaly;
                  var blood_columns = d3.keys(blood_anomaly[0]);
                  var urine_anomaly = scope.data.urine_anomaly;
                  var urine_columns = d3.keys(urine_anomaly[0]);
                  var urin = scope.data.urine;
                  var urin_columns = d3.keys(urin[0]);
                  var edta_anomaly = scope.data.edta_anomaly;
                  var edta_columns = d3.keys(edta_anomaly[0]);
                  var serum_anomaly = scope.data.serum_anomaly;
                  var serum_columns = d3.keys(serum_anomaly[0]);
                  var citrat_anomaly = scope.data.citrat_anomaly;
                  var citrat_columns = d3.keys(citrat_anomaly[0]);
                  var plausible = scope.data.plausible;
                  var plausible_columns = d3.keys(plausible[0]);
                  var NAs = scope.data.NAs;

             //overall data quality assessment report
             var plausible_caption = d3.select(root).append("caption").text("Overall-Plausibility-Report")
                             .attr("style", "margin-left: 250px");
                             //.style("border", "5px black solid");
             function createplausibletable(plausible, plausible_columns) {
                           var table9 = d3.select(root).append("table")
                           .attr("style", "margin-left: 250px")
                           .style("border", "2px black solid"),
                           thead9 = table9.append("thead"),
                           tbody9 = table9.append("tbody");

                     thead9.append("tr")
                           .selectAll("th")
                           .data(plausible_columns)
                           .enter()
                           .append("th")
                           .style("border", "1px black solid")
                           .style("padding", "5px")
                           .text(function(column) { return column; });
                           var rows9 = tbody9.selectAll("tr")
                                     .data(plausible)
                                     .enter()
                                     .append("tr");
                           var cells9 = rows9.selectAll("td")
                                    .data(function(row) {
                                       return plausible_columns.map(function(column) {
                                            return {column: column, value: row[column]};
                                                         });
                                                       })
                                                   .enter()
                                                   .append("td")
                                                   .attr("style", "ifont-family: Courier")
                                                   .style("border", "1px black solid")
                                                   .style("padding", "5px")
                                                   .html(function(d) { return d.value; });
                                                   return table9;
                                                   }

             createplausibletable(plausible, ["Features","Passed-Fields", "Failed-Fields", "Plausible_Rate"]);


              // blood state anomaly report
                  var blood_caption = d3.select(root).append("caption").text("Blood-State Anomaly")
                                                .attr("style", "margin-left: 250px");
                                                                    //.style("border", "5px black solid");
                    function createbloodtable(blood_anomaly, blood_columns) {
                                    var table2 = d3.select(root).append("table")
                                                   .attr("style", "margin-left: 250px")
                                                   .style("border", "2px black solid"),
                                                  thead2 = table2.append("thead"),
                                                  tbody2 = table2.append("tbody");
                                                  thead2.append("tr")
                                                        .selectAll("th")
                                                        .data(blood_columns)
                                                        .enter()
                                                        .append("th")
                                                        .style("border", "1px black solid")
                                                        .style("padding", "5px")
                                                        .text(function(column) { return column; });
                                    var rows2 = tbody2.selectAll("tr")
                                                      .data(blood_anomaly)
                                                      .enter()
                                                      .append("tr");
                                    var cells2 = rows2.selectAll("td")
                                                      .data(function(row) {
                                                        return blood_columns.map(function(column) {
                                                        return {column: column, value: row[column]};
                                                                     });
                                                             })
                                                       .enter()
                                                       .append("td")
                                                       .attr("style", "ifont-family: Courier")
                                                       .style("border", "1px black solid")
                                                       .style("padding", "5px")
                                                       .html(function(d) { return d.value; });
                                                                 return table2;
                                                                 }

                  createbloodtable(blood_anomaly, ["stateordinary","statelipaemic", "stateikteric", "statehaemolytic"]);

                  // urine state anomaly report
                  var urine_caption = d3.select(root).append("caption").text("Urine-State-Anomaly")
                                        .attr("style", "margin-left: 250px");
                                  //.style("border", "5px black solid");
                  function createurinetable(urine_anomaly, urine_columns) {
                                var table3 = d3.select(root).append("table")
                                .attr("style", "margin-left: 250px")
                                .style("border", "2px black solid"),
                                thead3 = table3.append("thead"),
                                tbody3 = table3.append("tbody");

                          thead3.append("tr")
                                .selectAll("th")
                                .data(urine_columns)
                                .enter()
                                .append("th")
                                .style("border", "1px black solid")
                                .style("padding", "5px")
                                .text(function(column) { return column; });
                                var rows3 = tbody3.selectAll("tr")
                                          .data(urine_anomaly)
                                          .enter()
                                          .append("tr");
                                var cells3 = rows3.selectAll("td")
                                         .data(function(row) {
                                            return urine_columns.map(function(column) {
                                                 return {column: column, value: row[column]};
                                                              });
                                                            })
                                                        .enter()
                                                        .append("td")
                                                        .attr("style", "ifont-family: Courier")
                                                        .style("border", "1px black solid")
                                                        .style("padding", "5px")
                                                        .html(function(d) { return d.value; });
                                                        return table3;
                                                        }

                  createurinetable(urine_anomaly, ["stateurinordinary","stateurinopaque", "stateurinbloody"]);

                  // edta anomaly report
                  var edta_caption = d3.select(root).append("caption").text("Edta-Anomaly")
                                  .attr("style", "margin-left: 250px");
                                  //.style("border", "5px black solid");
                  function createedtatable(edta_anomaly, edta_columns) {
                                var table4 = d3.select(root).append("table")
                                .attr("style", "margin-left: 250px")
                                .style("border", "2px black solid"),
                                thead4 = table4.append("thead"),
                                tbody4 = table4.append("tbody");

                          thead4.append("tr")
                                .selectAll("th")
                                .data(edta_columns)
                                .enter()
                                .append("th")
                                .style("border", "1px black solid")
                                .style("padding", "5px")
                                .text(function(column) { return column; });
                                var rows4 = tbody4.selectAll("tr")
                                          .data(edta_anomaly)
                                          .enter()
                                          .append("tr");
                                var cells4 = rows4.selectAll("td")
                                         .data(function(row) {
                                            return edta_columns.map(function(column) {
                                                 return {column: column, value: row[column]};
                                                              });
                                                            })
                                                        .enter()
                                                        .append("td")
                                                        .attr("style", "ifont-family: Courier")
                                                        .style("border", "1px black solid")
                                                        .style("padding", "5px")
                                                        .html(function(d) { return d.value; });
                                                        return table4;
                                                        }

                  createedtatable(edta_anomaly, ["numfulledta","numfullaliquotedta", "numaliquotedtacount"]);

                  // serum anomaly report
                  var serum_caption = d3.select(root).append("caption").text("Serum-Anomaly")
                                  .attr("style", "margin-left: 250px");
                                  //.style("border", "5px black solid");
                  function createserumtable(serum_anomaly, serum_columns) {
                                var table5 = d3.select(root).append("table")
                                .attr("style", "margin-left: 250px")
                                .style("border", "2px black solid"),
                                thead5 = table5.append("thead"),
                                tbody5 = table5.append("tbody");

                          thead5.append("tr")
                                .selectAll("th")
                                .data(serum_columns)
                                .enter()
                                .append("th")
                                .style("border", "1px black solid")
                                .style("padding", "5px")
                                .text(function(column) { return column; });
                                var rows5 = tbody5.selectAll("tr")
                                          .data(serum_anomaly)
                                          .enter()
                                          .append("tr");
                                var cells5 = rows5.selectAll("td")
                                         .data(function(row) {
                                            return serum_columns.map(function(column) {
                                                 return {column: column, value: row[column]};
                                                              });
                                                            })
                                                        .enter()
                                                        .append("td")
                                                        .attr("style", "ifont-family: Courier")
                                                        .style("border", "1px black solid")
                                                        .style("padding", "5px")
                                                        .html(function(d) { return d.value; });
                                                        return table5;
                                                        }

                  createserumtable(serum_anomaly, ["numfullserum","numfullaliquotserum", "numaliquotserumcount"]);

                  // citrat anomaly report
                  var citrat_caption = d3.select(root).append("caption").text("Citrat-Anomaly")
                                  .attr("style", "margin-left: 250px");
                                  //.style("border", "5px black solid");
                  function createcitrattable(citrat_anomaly, citrat_columns) {
                                var table6 = d3.select(root).append("table")
                                .attr("style", "margin-left: 250px")
                                .style("border", "2px black solid"),
                                thead6 = table6.append("thead"),
                                tbody6 = table6.append("tbody");

                          thead6.append("tr")
                                .selectAll("th")
                                .data(citrat_columns)
                                .enter()
                                .append("th")
                                .style("border", "1px black solid")
                                .style("padding", "5px")
                                .text(function(column) { return column; });
                                var rows6 = tbody6.selectAll("tr")
                                          .data(citrat_anomaly)
                                          .enter()
                                          .append("tr");
                                var cells6 = rows6.selectAll("td")
                                         .data(function(row) {
                                            return citrat_columns.map(function(column) {
                                                 return {column: column, value: row[column]};
                                                              });
                                                            })
                                                        .enter()
                                                        .append("td")
                                                        .attr("style", "ifont-family: Courier")
                                                        .style("border", "1px black solid")
                                                        .style("padding", "5px")
                                                        .html(function(d) { return d.value; });
                                                        return table6;
                                                        }

                  createcitrattable(citrat_anomaly, ["numfullcitrat","numfullaliquotcitrat", "numaliquotcitracount"]);

                  // urine sample anomaly report
                  var urin_caption = d3.select(root).append("caption").text("Urine-Anomaly")
                                  .attr("style", "margin-left: 250px");
                                  //.style("border", "5px black solid");
                  function createurintable(urin, urin_columns) {
                                var table7 = d3.select(root).append("table")
                                .attr("style", "margin-left: 250px")
                                .style("border", "2px black solid"),
                                thead7 = table7.append("thead"),
                                tbody7 = table7.append("tbody");

                          thead7.append("tr")
                                .selectAll("th")
                                .data(urin_columns)
                                .enter()
                                .append("th")
                                .style("border", "1px black solid")
                                .style("padding", "5px")
                                .text(function(column) { return column; });
                                var rows7 = tbody7.selectAll("tr")
                                          .data(urin)
                                          .enter()
                                          .append("tr");
                                var cells7 = rows7.selectAll("td")
                                         .data(function(row) {
                                            return urin_columns.map(function(column) {
                                                 return {column: column, value: row[column]};
                                                              });
                                                            })
                                                        .enter()
                                                        .append("td")
                                                        .attr("style", "ifont-family: Courier")
                                                        .style("border", "1px black solid")
                                                        .style("padding", "5px")
                                                        .html(function(d) { return d.value; });
                                                        return table7;
                                                        }

                  createurintable(urin, ["numfullurin","numfullaliquoturin", "numaliquoturincount"]);                 


        }
    }]);
