//# sourceURL=d3BarChart.js

'use strict';

window.smartRApp.directive('barchart', [
    'smartRUtils',
    'rServeService',
    function(smartRUtils, rServeService) {

        return {
            restrict: 'E',
            scope: {
                data: '=',
                width: '@',
                height: '@'
            },
            link: function (scope, element) {

                /**
                 * Watch data model (which is only changed by ajax calls when we want to (re)draw everything)
                 */
                scope.$watch('data', function() {
                    $(element[0]).empty();
                    if (! $.isEmptyObject(scope.data)) {
                        smartRUtils.prepareWindowSize(scope.width, scope.height);
                        createBarViz(scope, element[0]);
                    }
                });
            }
            };

       function createBarViz(scope, root) {
            var df = scope.data.catNAs;
	          var unknown = scope.data.unknown;

            var margin = {top: 30, right: 90, bottom: 120, left: 120},
          // var margin = {top: 20, right: 250, bottom: h / 4 + 230, left: w / 4},
                width = 960 - margin.left - margin.right,
                height = 500 - margin.top - margin.bottom;

            var w = d3.scale.category10();

            var x = d3.scale.ordinal()
                        .rangeRoundBands([0, width], .1);

            var y = d3.scale.linear()
                        .range([height, 0]);

            var xAxis = d3.svg.axis()
                          .scale(x)
                          .orient("bottom");
            var yAxis = d3.svg.axis()
                            .scale(y)
                            .orient("left");
                            //.tickFormat(formatPercent);

            // missing value barchart
            var svg = d3.select(root).append("svg")
                          .attr("width", width + margin.left + margin.right)
                          .attr("height", height + margin.top + margin.bottom)
                          .append("g")
                          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                          x.domain(df.map(function(d) { return d.variables; }));
                          y.domain([0, d3.max(df, function(d) { return d.missing_count; })]);

                          svg.append("g")
                              .attr("class", "x axis")
                              .attr("transform", "translate(0," + height + ")")
                              .call(xAxis)
	       		                  .selectAll("text")
	           	                .style("text-anchor", "end")
	           	                .attr("dx", "-.8em")
	                            .attr("dy", ".15em")
	                            .attr("transform", "rotate(-65)");
                          svg.append("g")
                              .attr("class", "y axis")
                              .call(yAxis)
                              .append("text")
                              .attr("transform", "rotate(-90)")
                              .attr("y", 6)
                              .attr("dy", ".71em")
                              .style("text-anchor", "end")
                              .text("Frequency");
                          svg.selectAll(".bar")
                              .data(df)
                              .enter().append("rect")
                              .attr("class", "bar")
                              .attr("x", function(d) { return x(d.variables); })
                              .attr("width", x.rangeBand())
                              .attr("y", function(d) { return y(d.missing_count); })
                              .attr("height", function(d) { return height - y(d.missing_count); });
	       		              svg.append("text")
	                           .attr("x", (width / 2))
	       		                 .attr("y", 0 -(margin.top / 2))
	                           .style("font-size", "18px")
	                           .style("text-decoration", "bold")
	                           .attr("text-anchor", "middle")
	                           .text("Missing Value Count");

	       // unknown values barchart
	       var svg2 = d3.select(root).append("svg")
	                                 .attr("width", width + margin.left + margin.right)
	                                 .attr("height", height + margin.top + margin.bottom)
	                                 .append("g")
	                                 .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	                                 x.domain(unknown.map(function(b) { return b.variables; }));
	                                 y.domain([0, d3.max(unknown, function(b) { return b.unknown_count; })]);

	                                 svg2.append("g")
	                                     .attr("class", "x axis")
	                                     .attr("transform", "translate(0," + height + ")")
	                                     .call(xAxis)
	                                     .selectAll("text")
	                                     .style("text-anchor", "end")
	                                     .attr("dx", "-.8em")
	                                     .attr("dy", ".15em")
	                                     .attr("transform", "rotate(-65)");
	                                 svg2.append("g")
	                                     .attr("class", "y axis")
	                                     .call(yAxis)
	                                     .append("text")
	                                     .attr("transform", "rotate(-90)")
	                                     .attr("y", 6)
	                                     .attr("dy", ".71em")
	                                     .style("text-anchor", "end")
	                                     .text("Frequency");
	       				                   svg2.selectAll(".bar")
	                                     .data(unknown)
	                                     .enter().append("rect")
	                                     .attr("class", "bar")
	                                     .attr("x", function(b) { return x(b.variables); })
	                                     .attr("width", x.rangeBand())
	                                     .attr("y", function(b) { return y(b.unknown_count); })
	                                     .attr("height", function(b) { return height - y(b.unknown_count); });
	       				                  svg2.append("text")
	                                    .attr("x", (width / 2))
	                                    .attr("y", 0 -(margin.top / 2))
	                                    .style("font-size", "18px")
	                                    .style("text-decoration", "bold")
	                                    .attr("text-anchor", "middle")
	                                    .text("Unknown-Uncollected Value Count");

                        function type(d) {
                                  d.na_count = +d.na_count;
                                  return d;
                                }
	       		            function type(b) {
                        				  b.unknown_count = +b.unknown_count;
                        				  return b;
                        				}

  }
  }
  ]);
